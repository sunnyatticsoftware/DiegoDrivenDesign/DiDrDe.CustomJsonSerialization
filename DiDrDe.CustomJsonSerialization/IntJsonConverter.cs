﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace DiDrDe.CustomJsonSerialization
{
    public class IntJsonConverter
        : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            int number = (int)value;
            var numberAsText = number.ToString(CultureInfo.InvariantCulture);
            writer.WriteValue("sadfaf");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = reader.Value;
            return 1;
        }

        public override bool CanRead => true;

        public override bool CanWrite => true;
        public override bool CanConvert(Type type) => type == typeof(int);
    }
}
