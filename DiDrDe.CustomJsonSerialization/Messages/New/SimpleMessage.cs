﻿using System;

namespace DiDrDe.CustomJsonSerialization.Messages.New
{
    public class SimpleMessage
    {
        public string Text { get; }

        public int Number { get; }
        public double AnotherNumber { get; }
        public SimpleMessage(string text, int number, double anotherNumber)
        {
            Text = text;
            Number = number;
            AnotherNumber = anotherNumber;
        }

        public DateTime CreatedOn { get; set; }
    }
}
