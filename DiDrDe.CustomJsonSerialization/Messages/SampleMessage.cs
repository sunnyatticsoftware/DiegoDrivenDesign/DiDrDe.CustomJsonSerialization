﻿namespace DiDrDe.CustomJsonSerialization.Messages
{
    public class SampleMessage
    {
        public string Text { get; }
        public int Number { get; }
        public SampleInner SampleInner { get; }

        public SampleMessage(string text, int number, SampleInner sampleInner)
        {
            Text = text;
            Number = number;
            SampleInner = sampleInner;
        }
    }
}
