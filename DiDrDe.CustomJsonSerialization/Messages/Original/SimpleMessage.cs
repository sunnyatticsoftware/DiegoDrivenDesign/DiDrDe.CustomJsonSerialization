﻿using DiDrDe.CustomJsonSerialization.Attributes;
using System;

namespace DiDrDe.CustomJsonSerialization.Messages.Original
{
    public class SimpleMessage
    {
        [PersonalData]
        public string Text { get; }

        [PersonalData]
        public int Number { get; }

        [PersonalData]
        public DateTime CreatedOn { get; }

        public SimpleMessage(string text, int number, DateTime createdOn)
        {
            Text = text;
            Number = number;
            CreatedOn = createdOn;
        }
    }
}
