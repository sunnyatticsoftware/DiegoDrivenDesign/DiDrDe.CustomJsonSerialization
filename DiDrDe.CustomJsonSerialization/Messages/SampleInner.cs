﻿namespace DiDrDe.CustomJsonSerialization.Messages
{
    public class SampleInner
    {
        public string AnotherText { get; }

        public int AnotherNumber { get; }
        public string SettableValue { get; set; }

        public SampleInner(string anotherText, int anotherNumber)
        {
            AnotherText = anotherText;
            AnotherNumber = anotherNumber;
        }
    }
}
