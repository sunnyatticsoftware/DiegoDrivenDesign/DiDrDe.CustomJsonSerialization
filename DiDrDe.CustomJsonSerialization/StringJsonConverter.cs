﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace DiDrDe.CustomJsonSerialization
{
    public class StringJsonConverter
        : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var path = reader.Path;
            var value = reader.Value;
            if (path != "number")
            {
                return value;
            }
            throw new NotImplementedException();
        }

        public override bool CanRead => true;

        public override bool CanWrite => false;
        public override bool CanConvert(Type type) => type == typeof(string);
    }
}
